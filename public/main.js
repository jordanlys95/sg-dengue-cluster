mapboxgl.accessToken = 'pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2thMjRoZjk5MDhkejNmbzV2NnQ3OGR2aCJ9.lKziTfam_bfJEQ43Eh3rig';

class PitchControl {
    onAdd(map) {
        this._map = map;
        const container = document.createElement('div');
        container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group';
        container.innerHTML = '<button class="mapboxgl-ctrl-icon mapboxgl-ctrl-custom-pitch" type="button"><div>3D</div></button>';
        container.onclick = function () {
            var pitch = map.getPitch();
            var nextPitch = 0;
            if (pitch < 30) {
                nextPitch = 30;
            } else if (pitch < 45) {
                nextPitch = 45;
            } else if (pitch < 60) {
                nextPitch = 60;
            }
            map.easeTo({ pitch: nextPitch });
        };
        map.on('pitchend', this.onPitch.bind(this));
        this._container = container;
        return this._container;
    }
    onPitch() {
        const pitch = this._map.getPitch();
        // // Custom 3D building feature when pitch is >0
        // if (pitch === 0){
        //     this._map.setLayoutProperty('3d-buildings', 'visibility', 'none');
        // } else {
        //     this._map.setLayoutProperty('3d-buildings', 'visibility', 'visible');

        // }
        // this._container.classList.toggle('active', !!pitch);
        const text = this._container.getElementsByTagName('div')[0];
        text.style.transform = 'rotate3d(1,0,0,' + pitch + 'deg)';
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map.off('pitchend', this.onPitch.bind(this));
        this._map = undefined;
    }
};


var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [103.8198, 1.3521],
    zoom: 10
});

map.addControl(new PitchControl(), "bottom-left")
map.addControl(new mapboxgl.NavigationControl(), "bottom-left");


map.on('load', function() {
    map.addSource('dengue_cluster', {
        'type': 'geojson',
        'data': 'dengue-cluster-2020-08-06.geojson'
    });
    map.addLayer({
        'id': 'dengue_cluster-line',
        'type': 'line',
        'source': 'dengue_cluster',
        'layout': {},
        'paint': {
            'line-color': [
                'case',
                ['==', ['get', 'case_number'], 0],
                '#00C853',
                ['<=', ['get', 'case_number'], 10],
                '#FF6D00',
                ['>', ['get', 'case_number'], 10],
                '#D50000',
                '#fff'
            ],
            'line-width': 1
        }
    });
    // map.addLayer({
    //     'id': 'dengue_cluster',
    //     'type': 'fill',
    //     'source': 'dengue_cluster',
    //     'layout': {},
    //     'paint': {
    //         'fill-color': [
    //             'case',
    //             ['==', ['get', 'case_number'], 0],
    //             '#00C853',
    //             ['<=', ['get', 'case_number'], 10],
    //             '#FF6D00',
    //             ['>', ['get', 'case_number'], 10],
    //             '#D50000',
    //             '#fff'
    //         ],
    //         'fill-opacity': 0.25
    //     }
    // });
    map.addLayer({
        'id': 'dengue_cluster-extrude',
        'type': 'fill-extrusion',
        'source': 'dengue_cluster',
        'layout': {},
        'paint': {
            'fill-extrusion-color': [
                'case',
                ['==', ['get', 'case_number'], 0],
                '#00C853',
                ['<=', ['get', 'case_number'], 10],
                '#FF6D00',
                ['>', ['get', 'case_number'], 10],
                '#D50000',
                '#fff'
            ],
            'fill-extrusion-height': ['get', 'case_number'],
            'fill-extrusion-base': 0,
            'fill-extrusion-opacity': 0.5
        }
    });

    map.on('click', 'dengue_cluster-extrude', function(e) {
        var coordinates = turf.centroid(turf.polygon(e.features[0].geometry.coordinates.slice()));
        var {LOCALITY, case_number} = e.features[0].properties;
        
        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
        
        new mapboxgl.Popup()
        .setLngLat(turf.getCoord(coordinates))
        .setHTML(`
            <h3>${case_number}</h3>
            <div>${LOCALITY}</div>
        `)
        .addTo(map);
    });
    
    // Change the cursor to a pointer when the mouse is over the places layer.
    map.on('mouseenter', 'places', function() {
    map.getCanvas().style.cursor = 'pointer';
    });
    
    // Change it back to a pointer when it leaves.
    map.on('mouseleave', 'places', function() {
    map.getCanvas().style.cursor = '';
    });
});
